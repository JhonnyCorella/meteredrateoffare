﻿using System.Web.Http;
using MeteredRateOfFareCommon.Interfaces;
using MeteredRateOfFareCommon.Models;
using MeteredRateOfFareCommon.Utilities;

namespace MeteredRateOfFareServices.Controllers
{
    public class RateCalculatorController : ApiController
    {
        [HttpPost]
        public IHttpActionResult CalculateRate([FromBody] Ride ride)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            if (ride != null)
            {
                IRateCalculator rateCalculator = new RateCalculator(ride);
                rateCalculator.CalculateRideRate();
            }

            return Ok(ride);
        }
    }
}