﻿namespace MeteredRateOfFareCommon.Interfaces
{
    public interface IRateCalculator
    {
        IRide CalculateRideRate();
    }
}
