﻿using System;

namespace MeteredRateOfFareCommon.Interfaces
{
    public interface IRide
    {
        DateTime DateRide { get; set; }
        TimeSpan TimeStartRide { get; set; }
        double MinutesAboveSixMPH { get; set; }
        double MilesBelowSixMPH { get; set; }
        double TotalRate { get; set; }
    }
}
