﻿using System;
using MeteredRateOfFareCommon.Interfaces;

namespace MeteredRateOfFareCommon.Models
{
    public class Ride : IRide
    {
        public DateTime DateRide { get; set; }
        public TimeSpan TimeStartRide { get; set; }
        public double MinutesAboveSixMPH { get; set; }
        public double MilesBelowSixMPH { get; set; }
        public double TotalRate { get; set; }
    }
}
