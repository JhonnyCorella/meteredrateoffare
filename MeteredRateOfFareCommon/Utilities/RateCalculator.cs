﻿using System;
using MeteredRateOfFareCommon.Interfaces;

namespace MeteredRateOfFareCommon.Utilities
{
    public class RateCalculator : IRateCalculator
    {
        /// <summary>
        /// Upon entry rate = $3.00
        /// </summary>
        private const double UponEntryRate = 3.00;

        /// <summary>
        /// Additional unit fare = $0.35
        /// </summary>
        private const double AdditionalUnitFare = 0.35;

        /// <summary>
        /// This is 1/5 of mile representation
        /// </summary>
        private const double OneFifthOfMile = 0.2;

        /// <summary>
        /// Not in motion or traveling at 6 miles per hour or more fare = 60 seconds
        /// </summary>
        private const int NotInMotionOrAboveSixMphFare = 60;

        /// <summary>
        /// Night surcharge between 8:00 PM & 6:00 AM fare = $0.50
        /// </summary>
        private const double NightSurchargeFare = 0.50;

        /// <summary>
        /// Peak hour Weekday Surcharge. Monday - Friday between 4:00 PM & 8:00 PM fare = $1.00
        /// </summary>
        private const double PeakHourWeekdaySurchargeFare = 1.00;

        /// <summary>
        /// New York State Tax Surcharge per ride fare = $0.50
        /// </summary>
        private const double NewYorkStateTaxSurchargeFare = 0.50;

        private IRide _ride;
        public IRide Ride
        {
            get { return _ride; }
            set
            {
                _ride = value;
            }
        }

        /// <summary>
        /// Rate calculator constructor
        /// </summary>
        /// <param name="ride"></param>
        public RateCalculator(IRide ride)
        {
            _ride = ride;
        }

        /// <summary>
        /// Method used to calculate the ride rate according to fare's rules.
        /// </summary>
        /// <returns>Total rate of taxicab</returns>
        public IRide CalculateRideRate()
        {
            var result = new double();

            result += GetUponEntryRate();
            result += GetTravelBelowSixMPHRate(this.Ride.MilesBelowSixMPH);
            result += GetTravelAboveSixMPHRate(this.Ride.MinutesAboveSixMPH);
            result += GetNightSurchargeRate(this.Ride.TimeStartRide);
            result += GetPeakHourWeekdaySurchargeRate(this.Ride.DateRide, this.Ride.TimeStartRide);
            result += GetNewYorkTaxSurchargeRate();

            this.Ride.TotalRate = result;
            return this.Ride;
        }

        private static double GetUponEntryRate()
        {
            const double result = UponEntryRate;

            return result;
        }

        private static double GetTravelBelowSixMPHRate(double milesBelowSixMPH)
        {
            var result = 0.0;
            if (!(milesBelowSixMPH > 0))
                return result;

            var oneFifthsOfMile = milesBelowSixMPH / OneFifthOfMile;
            result = oneFifthsOfMile * AdditionalUnitFare;

            return result;
        }

        private static double GetTravelAboveSixMPHRate(double minutesAboveSixMPH)
        {
            var result = 0.0;

            if (!(minutesAboveSixMPH > 0))
                return result;

            var secondsNotMotionOrAboveSixMph = (minutesAboveSixMPH * 60) / NotInMotionOrAboveSixMphFare;
            result = secondsNotMotionOrAboveSixMph * AdditionalUnitFare;

            return result;
        }

        private static double GetNightSurchargeRate(TimeSpan time)
        {
            var result = new double();
            var minimumHour = new TimeSpan(20, 0, 0);    // 8:00 pm
            var maximumHour = new TimeSpan(6, 0, 0);     // 6:00 am

            if (time < minimumHour && time > maximumHour)
                return result;

            const double nightSurcharge = NightSurchargeFare;
            result = nightSurcharge;

            return result;
        }

        private static double GetPeakHourWeekdaySurchargeRate(DateTime dateTime, TimeSpan time)
        {
            var result = 0.0;
            var minimumHour = new TimeSpan(16, 0, 0);   // 4:00 pm
            var maximumHour = new TimeSpan(20, 0, 0);   // 8:00 am

            //Validate if time is peak hour
            if (time < minimumHour || time > maximumHour)
                return result;

            //Validate if day is not weekend
            if (dateTime.DayOfWeek == DayOfWeek.Sunday || dateTime.DayOfWeek == DayOfWeek.Saturday)
                return result;

            const double peakHourWeekdaySurcharge = PeakHourWeekdaySurchargeFare;
            result = peakHourWeekdaySurcharge;

            return result;
        }

        private static double GetNewYorkTaxSurchargeRate()
        {
            var result = 0.0;

            result = NewYorkStateTaxSurchargeFare;

            return result;
        }
    }
}
