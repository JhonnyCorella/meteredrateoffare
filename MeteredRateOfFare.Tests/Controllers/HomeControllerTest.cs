﻿using System.Web.Mvc;
using MeteredRateOfFare.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeteredRateOfFare.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index() as ViewResult; 

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
