﻿using System;
using System.Web.Http.Results;
using MeteredRateOfFareCommon.Models;
using MeteredRateOfFareServices.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeteredRateOfFareServices.Tests
{
    [TestClass]
    public class RateCalculatorControllerTest
    {
        [TestMethod]
        public void CalculateRate_ValidExampleRateAmount_AmountCalculatedCorrectly()
        {
            //Arrange
            var ride = new Ride { DateRide = new DateTime(2010, 10, 8), TimeStartRide = new TimeSpan(17, 30, 0), MinutesAboveSixMPH = 5, MilesBelowSixMPH = 2 };
            var sut = new RateCalculatorController();
            var expectedRate = 9.75;

            //Act
            var result = sut.CalculateRate(ride) as OkNegotiatedContentResult<Ride>;
            var rideResult = result.Content as Ride;

            //Assert
            Assert.AreEqual(expectedRate, rideResult.TotalRate);
        }

        [TestMethod]
        public void CalculateRate_ValidNoTravelBelowSixMPHRateAmount_AmountCalculatedCorrectly()
        {
            //Arrange
            var ride = new Ride { DateRide = new DateTime(2010, 10, 8), TimeStartRide = new TimeSpan(17, 30, 0), MinutesAboveSixMPH = 5, MilesBelowSixMPH = 0 };
            var sut = new RateCalculatorController();
            var expectedRate = 6.25;

            //Act
            var result = sut.CalculateRate(ride) as OkNegotiatedContentResult<Ride>;
            var rideResult = result.Content as Ride;

            //Assert
            Assert.AreEqual(expectedRate, rideResult.TotalRate);
        }

        [TestMethod]
        public void CalculateRate_ValidNoTravelAboveSixMPHRateAmount_AmountCalculatedCorrectly()
        {
            //Arrange
            var ride = new Ride { DateRide = new DateTime(2010, 10, 8), TimeStartRide = new TimeSpan(17, 30, 0), MinutesAboveSixMPH = 5, MilesBelowSixMPH = 0 };
            var sut = new RateCalculatorController();
            var expectedRate = 6.25;

            //Act
            var result = sut.CalculateRate(ride) as OkNegotiatedContentResult<Ride>;
            var rideResult = result.Content as Ride;

            //Assert
            Assert.AreEqual(expectedRate, rideResult.TotalRate);
        }

        [TestMethod]
        public void CalculateRate_ValidTravelAtNightAndNoPeakHourRateAmount_AmountCalculatedCorrectly()
        {
            //Arrange
            var ride = new Ride { DateRide = new DateTime(2010, 10, 8), TimeStartRide = new TimeSpan(20, 30, 0), MinutesAboveSixMPH = 5, MilesBelowSixMPH = 2 };
            var sut = new RateCalculatorController();
            var expectedRate = 9.25;

            //Act
            var result = sut.CalculateRate(ride) as OkNegotiatedContentResult<Ride>;
            var rideResult = result.Content as Ride;

            //Assert
            Assert.AreEqual(expectedRate, rideResult.TotalRate);
        }

        [TestMethod]
        public void CalculateRate_ValidTravelAtNotPeakHourRateAmount_AmountCalculatedCorrectly()
        {
            //Arrange
            var ride = new Ride { DateRide = new DateTime(2010, 10, 8), TimeStartRide = new TimeSpan(13, 30, 0), MinutesAboveSixMPH = 5, MilesBelowSixMPH = 2 };
            var sut = new RateCalculatorController();
            var expectedRate = 8.75;

            //Act
            var result = sut.CalculateRate(ride) as OkNegotiatedContentResult<Ride>;
            var rideResult = result.Content as Ride;

            //Assert
            Assert.AreEqual(expectedRate, rideResult.TotalRate);
        }
    }
}
