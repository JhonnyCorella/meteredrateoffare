﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Web.Mvc;
using MeteredRateOfFareCommon.Models;
using Newtonsoft.Json;

namespace MeteredRateOfFare.Controllers
{
    public class HomeController : Controller
    {
        private const string CalculateRatePath = @"RateCalculator/CalculateRate";

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Method used to call service to calculate the ride rate
        /// </summary>
        /// <param name="ride"></param>
        /// <returns></returns>
        public JsonResult CalculateRideRate(Ride ride)
        {
            using (var client = new HttpClient())
            {
                var apiUri = ConfigurationManager.AppSettings["UriApi"];
                var uri = string.Format("{0}{1}", apiUri, CalculateRatePath);

                client.BaseAddress = new Uri(uri);

                var postTask = client.PostAsJsonAsync<Ride>("ride", ride as Ride);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var resultString = result.Content.ReadAsStringAsync();
                    var rideJsonResult = resultString.Result;
                    var rideResult = JsonConvert.DeserializeObject<Ride>(rideJsonResult);
                    ride = rideResult;
                }
            }

            return Json(ride, JsonRequestBehavior.AllowGet);
        }
    }
}