﻿app.controller('HomeControllerJS', ['$scope', 'homeService',
    function ($scope, homeService) {
        $scope.DateRide = "";
        $scope.TimeStartRide = "";
        $scope.MinutesAboveSixMPH = "";
        $scope.MilesBelowSixMPH = "";
        $scope.TotalRate = "";

        $scope.calculateRideRate = function () {
                var rideRateToCalculate = {
                    DateRide: $scope.DateRide,
                    TimeStartRide: $scope.TimeStartRide.getHours() + ':' + $scope.TimeStartRide.getMinutes(),
                    MinutesAboveSixMPH: $scope.MinutesAboveSixMPH,
                    MilesBelowSixMPH: $scope.MilesBelowSixMPH
                };
                homeService.CalculateRideRate(rideRateToCalculate)
                    .then(function (result) {
                        $scope.TotalRate = result.data.TotalRate;
                            console.log('Segment updated successfully...');
                        },
                        function (msg) {
                            console.log('There was an error while saving the Segment...', msg);
                        });
            };
        }
    ]
);

app.factory('homeService',
    [
        '$http', function ($http) {

            var homeService = {};

            homeService.CalculateRideRate = function (postObject) {
                return $http.post("/Home/CalculateRideRate", angular.toJson(postObject));
            };
            
            return homeService;
        }
    ]);